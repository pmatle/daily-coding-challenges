﻿''' <summary>
''' This problem was asked by Microsoft.
''' 
''' Given a number In the form Of a list Of digits, Return all possible permutations.
''' For example:
'''             given [1,2,3]
'''             return [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]].
''' </summary>
Public Class Permuting
    Inherits Permutation

    Public Sub New(ByVal list As List(Of Integer))
        MyBase.New(list)
    End Sub

    ''' <summary>
    ''' Calculates the factorial of a number.
    ''' </summary>
    ''' <param name="num">Number of the factorial we are looking for</param>
    ''' <returns>Factorial of the parameter num</returns>
    Public Function Factorial(ByVal num As Integer) As Integer
        If num = 0 Then
            Return 1
        End If

        Return num * Factorial(num - 1)
    End Function

    ''' <summary>
    ''' Calculates the total number of permutations possible for a number.
    ''' </summary>
    ''' <param name="list">List containing all our digits/objects</param>
    ''' <param name="sample">Number of digits/objects in the list</param>
    ''' <returns>Total number of permutation</returns>
    Public Function GetNumberOfPermutation(ByVal list As List(Of Integer), ByVal sample As Integer) As Integer
        Return Factorial(list.Count()) / Factorial(list.Count() - sample)
    End Function

    ''' <summary>
    ''' Print a list of digits representing a number.
    ''' </summary>
    ''' <param name="list">List of digits</param>
    Public Sub PrintPermutation(ByVal list As List(Of Integer))
        Dim index As Integer

        For index = 0 To list.Count() - 1
            Console.Write(list(index))
        Next
        Console.WriteLine()
    End Sub

    ''' <summary>
    ''' Prints all the permutations of a particular number represented as a list.
    ''' </summary>
    ''' <param name="list"></param>
    Public Sub PrintAllPermutations(ByVal list As List(Of List(Of Integer)))
        Dim index As Integer

        For index = 0 To list.Count() - 1
            PrintPermutation(list(index))
        Next
    End Sub

    ''' <summary>
    ''' Gets a list of all the permutations of a number.
    ''' </summary>
    Public Function GetAllPermutations() As List(Of List(Of Integer))
        Dim count As Integer
        Dim totalPermutations As Integer
        Dim permutation As List(Of Integer)
        Dim permutationList As New List(Of List(Of Integer))

        permutation = Number
        permutationList.Add(Number.ToList())
        totalPermutations = GetNumberOfPermutation(Number, Number.Count())
        For count = 0 To totalPermutations - 2
            permutation = FindNextPermutation()
            permutationList.Add(permutation.ToList())
            Number = permutation
        Next

        Return permutationList
    End Function

End Class
