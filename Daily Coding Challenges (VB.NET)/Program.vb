Imports System

Module Program
    Sub Main(args As String())
        Dim list As New List(Of Integer)(New Integer() {1, 2, 3})
        Dim permutation As New Permuting(list)

        Dim result = permutation.GetAllPermutations()
        permutation.PrintAllPermutations(result)
        Console.ReadLine()
    End Sub
End Module
