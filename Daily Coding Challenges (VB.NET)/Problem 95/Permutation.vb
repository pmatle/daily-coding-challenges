﻿
''' <summary>
''' This problem was asked by Palantir.
''' 
''' Given a number represented by a list Of digits, find the Next greater permutation of that number, In terms Of lexicographic ordering.
''' If there is no greater permutation possible, Return the permutation with the lowest value/ordering.
''' 
''' For example:
'''                 The list [1,2,3] should return [1,3,2].
'''                 The list [1,3,2] should return [2,1,3].
'''                 The list [3,2,1] should return [1,2,3].
''' </summary>
Public Class Permutation

    Public Sub New(ByVal list As List(Of Integer))
        Number = list
    End Sub

    ''' <summary>
    ''' Gets and sets the number as a list of digits.
    ''' </summary>
    ''' <returns>List of digits representing a number</returns>
    Public Property Number As List(Of Integer)

    ''' <summary>
    ''' Swaps to numbers using XOR bitwise operation.
    ''' </summary>
    ''' <param name="one">First number</param>
    ''' <param name="two">Second number</param>
    Private Sub Swap(ByRef one As Integer, ByRef two As Integer)
        one = one Xor two
        two = two Xor one
        one = one Xor two
    End Sub

    ''' <summary>
    ''' Reverses the list from the index given up until the end of the list.
    ''' </summary>
    ''' <param name="list">The list to be reversed from index</param>
    ''' <param name="pos">The index from which the reversing should start</param>
    Private Sub ReverseFromN(ByRef list As List(Of Integer), ByVal pos As Integer)
        Dim forwardCur As Integer
        Dim backwardCur As Integer
        Dim length As Integer

        length = list.Count() - pos
        backwardCur = list.Count() - 1
        For forwardCur = pos To length / 2
            Swap(list(forwardCur), list(backwardCur))
            backwardCur -= 1
        Next
    End Sub

    ''' <summary>
    ''' Finds the next greater permutation of a list, in terms of lexicographic ordering.
    ''' If there is no greater permutation possible, it returns the permutation with the lowest ordering.
    ''' </summary>
    ''' <returns>List containing the permutation</returns>
    Public Function FindNextPermutation() As List(Of Integer)
        Dim x As Integer
        Dim y As Integer

        For x = Number.Count() - 2 To 0 Step -1
            If Number(x) < Number(x + 1) Then
                Exit For
            End If
        Next

        If x = -1 Then
            ReverseFromN(Number, 0)
            Return Number
        End If

        For y = Number.Count() - 1 To x + 1 Step -1
            If Number(x) < Number(y) Then
                Swap(Number(x), Number(y))
                ReverseFromN(Number, x + 1)
                Exit For
            End If
        Next

        Return Number
    End Function

End Class
